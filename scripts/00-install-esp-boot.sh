#!/bin/bash

set -o pipefail -o noclobber -o nounset -e

export DISK=/dev/disk/by-id/virtio-SSD-01

ESP_SIZE="512M"
SWAP_SIZE="$(awk '/MemTotal/ {print $2}' /proc/meminfo)K"
echo "Using $DISK, ESP $ESP_SIZE, swap $SWAP_SIZE"
sleep 2

sgdisk  --zap-all                     $DISK
sgdisk  -n1:24K:+$ESP_SIZE   -t1:EF00 $DISK
sgdisk  -n2:0:-"$SWAP_SIZE"  -t2:BF01 $DISK
sgdisk  -n3:0:0              -t3:8200 $DISK

fdisk -l $DISK
sleep 1

# Format ESP
echo "Format ESP"
mkfs.fat -F32 ${DISK}-part1

echo "mkswap"
mkswap $DISK-part3

# rpoolpass
echo "Create encrypted LUKS volume"

cryptsetup -y -v luksFormat --type luks2 ${DISK}-part2
cryptsetup open ${DISK}-part2 root

mkfs.btrfs --label btrfsroot /dev/mapper/root
mkdir /btrfs
mount /dev/mapper/root /btrfs

./05-create-subvols.sh

echo "Mount BTRFS subvols"
mount -o subvol=@ /dev/mapper/root /mnt
mkdir -p /mnt/home
mount -o subvol=@home /dev/mapper/root /mnt/home
mkdir -p /mnt/srv
mount -o subvol=@srv /dev/mapper/root /mnt/srv
mkdir -p /mnt/.snapshots
mount -o subvol=@snapshots /dev/mapper/root /mnt/.snapshots


echo "Mount ESP to /mnt/boot"
mkdir /mnt/boot
mount ${DISK}-part1 /mnt/boot

# Install arch
echo "Installing Arch"
pacstrap /mnt $(<packages)

echo "Done, ready for chroot"
