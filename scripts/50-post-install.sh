#!/bin/bash

set -o pipefail -o noclobber -o nounset -e

if [[ $EUID == 0 ]]; then
    echo "Do not run as root"
    exit 1
fi

sudo pacman -Syu --needed $(<packages-opt)

echo "Install pikaur"
git clone https://aur.archlinux.org/pikaur.git
cd pikaur
makepkg -sifr

echo "Initialize pgsql database"
# PGSQL
sudo -u postgres initdb -D /var/lib/postgres/data

echo "Install AUR packages"
pikaur -S --needed $(<packages-aur)

# podman
# set driver = "zfs"
# comment out mountopt
