#!/bin/bash

pacman -S clevis tpm2-tools luksmeta libpwquality

clevis luks bind -d $DISK-part2 tpm2 '{"pcr_bank":"sha256","pcr_ids":"0,1,7"}'

# cryptsetup luksDump $DISK-part2

cat << EOF > /etc/systemd/system/initrd-clevis.service
[Unit]
Description=Unlock rootfs with clevis
Documentation=man:clevis-luks-unlock(1)
ConditionPathExists=/etc/initrd-release
DefaultDependencies=no
After=initrd-shell.service
After=cryptsetup-pre.target
Before=cryptsetup.target initrd-cryptsetup.service
Requires=initrd-shell.service

[Service]
ExecStart=/usr/bin/clevis luks unlock -d /dev/disk/by-uuid/$(lsblk -dno UUID $DISK-part2)

[Install]

[X-SystemdTool]
InitrdBinary=/usr/bin/clevis-decrypt-tang
InitrdBinary=/usr/bin/clevis-decrypt-sss
InitrdBinary=/usr/bin/clevis-decrypt
InitrdBinary=/usr/bin/clevis-decrypt-tpm2
InitrdBinary=/usr/bin/tpm2_createprimary
InitrdBinary=/usr/bin/tpm2_unseal
InitrdBinary=/usr/bin/tpm2_load

InitrdPath=/usr/lib/libtss2-tcti-device.so.0.0.0
InitrdLink=/usr/lib/libtss2-tcti-device.so.0.0 target=/usr/lib/libtss2-tcti-device.so.0.0.0
InitrdLink=/usr/lib/libtss2-tcti-device.so.0 target=/usr/lib/libtss2-tcti-device.so.0.0.0

InitrdBinary=/usr/bin/luksmeta
InitrdBinary=/usr/bin/jose
InitrdBinary=/usr/bin/clevis
InitrdBinary=/usr/bin/curl
InitrdBinary=/usr/bin/awk
InitrdBinary=/usr/bin/bash

InitrdPath=/usr/lib/mkinitcpio-systemd-tool/secret.jwe
InitrdCall=add_all_modules /char/tpm/
EOF

systemctl enable initrd-clevis.service

# Using initrd-shell.sh
echo "<pass>" | clevis encrypt tpm2 '{"pcr_bank":"sha256","pcr_ids":"0,1,7"}' > /usr/lib/mkinitcpio-systemd-tool/secret.jwe
chmod 600 /usr/lib/mkinitcpio-systemd-tool/secret.jwe