#!/bin/bash

set -o pipefail -o nounset -e

var_user=andrei
var_hostname="archVM"
var_timezone="Europe/Oslo"
SYSTEMD_TOOL=0
BUSYBOX_INIT=0
DRACUT=0

read -p "Use dracut? " reply
case $reply in
    [Yy]* ) DRACUT=1;;
    * ) DRACUT=0;;
esac

if [[ $DRACUT -ne 1 ]]; then
    read -p "Use systemd-tool? " reply
    case $reply in
        [Yy]* ) SYSTEMD_TOOL=1;;
        * ) SYSTEMD_TOOL=0;;
    esac
    if [[ $SYSTEMD_TOOL -ne 1 ]]; then
        read -p "Use busybox init? " reply
        case $reply in
            [Yy]* ) BUSYBOX_INIT=1;;
            * ) BUSYBOX_INIT=0;;
        esac
    fi
fi

echo "Set hostname"
hostnamectl set-hostname "$var_hostname"

ln -sf /usr/share/zoneinfo/"$var_timezone" /etc/localtime
hwclock --systohc

echo "Setting locale"
sed -i '/^#en_US\.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
sed -i '/^#en_GB\.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
sed -i '/^#nb_NO\.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
locale-gen

cat <<EOF > /etc/locale.conf
LANG=en_US.UTF-8
LC_TIME=en_GB.UTF-8
EOF

echo "Set /etc/hosts"
cat <<EOF > /etc/hosts
127.0.0.1   localhost
::1     localhost
EOF

# Network setup
echo "systemd-networkd setup"
systemctl enable systemd-networkd.service systemd-resolved.service
primary_inet=$(route | grep '^default' | grep -m 1 -o '[^ ]*$')
echo "Enable DHCP for primary network adapter: ${primary_inet}"
cat <<EOF > /etc/systemd/network/20-wired.network
[Match]
Name=$primary_inet

[Network]
DHCP=yes
EOF

echo "Some default /etc/profile options"
cat <<EOF > /etc/profile.d/mine.sh
export EDITOR='vim'
export VISUAL='vim'
export PAGER='less'
alias ll='ls -lh'
alias la='ls -lha'
EOF

echo "ZSH /etc/zsh/zprofile"
cat <<EOF >> /etc/zsh/zprofile
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/site-functions
EOF

echo "Enable pacman verbose output and color"
sed -i '/VerbosePkgLists/s/^#//g' /etc/pacman.conf
sed -i '/Color/s/^#//g' /etc/pacman.conf

chsh -s /usr/bin/zsh

echo "Create $var_user user"
useradd -s /usr/bin/zsh -d /home/${var_user} -G wheel ${var_user}
cp -a /etc/skel/. /home/${var_user}

# Set root password
echo "Set root password"
passwd

echo "Set andrei password"
passwd andrei

echo "Allow wheel group access to sudo"
sed -i '0,/^# %wheel ALL=(ALL) ALL/s//%wheel ALL=(ALL) ALL/' /etc/sudoers

echo "Installing systemd-boot to /boot"
bootctl --path=/boot install

# Common kernel options
kernel_opts="rw nmi_watchdog=0"

echo "loader.conf, default to normal kernel"
cat <<EOF > /boot/loader/loader.conf
default arch
timeout 2
console-mode max
editor yes
EOF

BTRFS_OPTS=ssd,discard=async,compress-force=zstd:2,space_cache=v2

echo "Set /etc/fstab"
cat <<EOF > /etc/fstab
# Static information about the filesystems.
# See fstab(5) for details.

# <file system> <dir> <type> <options> <dump> <pass>
UUID=$(lsblk -dno UUID /dev/mapper/root)    /               btrfs     subvol=@,${BTRFS_OPTS}             0   0
UUID=$(lsblk -dno UUID /dev/mapper/root)    /.snapshots     btrfs     subvol=@snapshots,${BTRFS_OPTS}    0   0
UUID=$(lsblk -dno UUID /dev/mapper/root)    /home           btrfs     subvol=@home,${BTRFS_OPTS}         0   0
UUID=$(lsblk -dno UUID /dev/mapper/root)    /srv            btrfs     subvol=@srv,${BTRFS_OPTS}          0   0
UUID=$(lsblk -dno UUID $DISK-part1)         /boot           vfat      rw,noatime,fmask=027,dmask=027,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro    0   2
UUID=$(lsblk -dno UUID $DISK-part3)         none            swap      defaults    0   0
EOF

if [[ $DRACUT -eq 1 ]] ; then
    echo "Installing dracut"
    pacman -S --noconfirm --needed dracut
    if [[ -f /etc/pacman.d/hooks/90-mkinitcpio-install.hook ]]; then
        echo "Disable mkinitcpio"
        ln -sf /dev/null /etc/pacman.d/hooks/90-mkinitcpio-install.hook
        ln -sf /dev/null /etc/pacman.d/hooks/60-mkinitcpio-remove.hook
    fi
    echo "Installing pacman hooks"
    sudo -u nobody git clone https://aur.archlinux.org/dracut-hook.git /tmp/dracut-hook
    cd /tmp/dracut-hook
    sudo -u nobody makepkg
    pacman -U --noconfirm ./dracut-hook*.tar.*
else
    echo "Installing mkinitcpio"
    pacman -S --noconfirm --needed mkinitcpio
    
    echo "Disable fallback initramfs"
    sed -i $'s/^PRESETS=.*/PRESETS=\(\'default\'\)/g' /etc/mkinitcpio.d/linux.preset
    sed -i $'s/^PRESETS=.*/PRESETS=\(\'default\'\)/g' /etc/mkinitcpio.d/linux-lts.preset
fi

if [[ $SYSTEMD_TOOL -eq 1 ]]; then
    echo "initramfs setup"
    sed -i 's/^HOOKS=.*/HOOKS=\(base autodetect modconf block filesystems keyboard fsck systemd systemd-tool\)/g' /etc/mkinitcpio.conf

    pacman -S --noconfirm --needed mkinitcpio-systemd-tool tinyssh tinyssh-convert

    echo "Set [initrd]/etc/fstab"
    cat <<EOF >> /etc/mkinitcpio-systemd-tool/config/fstab
/dev/mapper/root    /sysroot    btrfs     subvol=@,${BTRFS_OPTS},x-systemd.device-timeout=9999h     0   0
EOF

    echo "Set [initrd]/etc/crypttab"
    cat <<EOF >> /etc/mkinitcpio-systemd-tool/config/crypttab
root    UUID=$(lsblk -dno UUID $DISK-part2)     none    luks
EOF

    echo "Change tinyssh port to 2222"
    mkdir -p /etc/systemd/system/initrd-tinysshd.service.d
    cat <<EOF >> /etc/systemd/system/initrd-tinysshd.service.d/override.conf
[Service]
Environment=SSHD_PORT=2222
EOF

    echo "Enable initrd units"
    systemctl enable initrd-cryptsetup.path
    systemctl enable initrd-tinysshd.service
    systemctl enable initrd-sysroot-mount.service

elif [[ $DRACUT -ne 1 ]]; then
    echo "initramfs setup"
    if [[ $BUSYBOX_INIT -eq 1 ]]; then
        sed -i 's/^HOOKS=.*/HOOKS=\(base udev autodetect keyboard keymap consolefont modconf block encrypt filesystems\)/g' /etc/mkinitcpio.conf
        kernel_opts+=" cryptdevice=UUID=$(lsblk -dno UUID $DISK-part2):root root=/dev/mapper/root rootflags=subvol=@"
    else
        # systemd init
        sed -i 's/^HOOKS=.*/HOOKS=\(base systemd autodetect keyboard sd-vconsole modconf block sd-encrypt filesystems\)/g' /etc/mkinitcpio.conf
        kernel_opts+=" rd.luks.name=$(lsblk -dno UUID $DISK-part2)=root root=/dev/mapper/root rootflags=subvol=@"
    fi
fi

echo "Boot loader entries"
cat <<EOF > /boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
EOF

cat <<EOF > /boot/loader/entries/arch-lts.conf
title   Arch Linux (LTS)
linux   /vmlinuz-linux-lts
EOF

if [[ $DRACUT -eq 1 ]] ; then
    kernel_opts+=" rd.neednet=1 ip=dhcp rootfstype=btrfs rootflags=subvol=@"
    echo "arch.conf"
    echo "initrd  /initramfs-linux.img" >> /boot/loader/entries/arch.conf
    echo "options $kernel_opts" >> /boot/loader/entries/arch.conf
    echo "arch-lts.conf"
    echo "initrd  /initramfs-linux-lts.img" >> /boot/loader/entries/arch-lts.conf
    echo "options $kernel_opts" >> /boot/loader/entries/arch-lts.conf
else
    echo "arch.conf"
    echo "initrd  /initramfs-linux.img" >> /boot/loader/entries/arch.conf
    echo "initrd  /intel-ucode.img" >> /boot/loader/entries/arch.conf
    echo "options $kernel_opts" >> /boot/loader/entries/arch.conf
    echo "arch-lts.conf"
    echo "initrd  /initramfs-linux-lts.img" >> /boot/loader/entries/arch-lts.conf
    echo "initrd  /intel-ucode.img" >> /boot/loader/entries/arch-lts.conf
    echo "options $kernel_opts" >> /boot/loader/entries/arch-lts.conf
fi

echo "Enable ssh"
systemctl enable sshd
echo "Change default target to multi-user"
systemctl set-default multi-user.target

if [[ $DRACUT -eq 1 ]] ; then
    echo "Build initramfs for all kernels"
    for line in /usr/lib/modules/*; do
        kver="${line#'/usr/lib/modules/'}"
        if [[ $kver == *"-lts"* ]]; then
            echo dracut --hostonly --no-hostonly-cmdline -f /boot/initramfs-linux-lts.img --kver "$kver"
        else
            echo dracut --hostonly --no-hostonly-cmdline -f /boot/initramfs-linux.img --kver "$kver"
        fi
    done
else
    mkinitcpio -P
fi

echo "Check fstab"
sleep 2
vim /etc/fstab

echo "Snapshot initial install"
btrfs subvolume snapshot -r / /.snapshots/install
