#!/bin/bash

set -o pipefail -o noclobber -e

if [[ -z $DISK ]]; then
    echo "Set DISK variable before running"
    exit 1
fi

echo "Copying scripts"
cp -r . /mnt/root/install-scripts

if [[ -f /root/.ssh/authorized_keys ]]; then
    echo "Copy root ssh key"
    mkdir /mnt/root/.ssh/
    cp /root/.ssh/authorized_keys /mnt/root/.ssh/authorized_keys
    chmod 700 /mnt/root/.ssh
    chmod 600 /mnt/root/.ssh/authorized_keys
fi

echo "Read-only snapshot before chroot"
btrfs subvolume snapshot -r /mnt /mnt/.snapshots/pre-chroot

echo "Enter chroot"
arch-chroot /mnt /usr/bin/env DISK=$DISK bash --login
