#!/bin/sh

set -e

echo "Create root subvolume"
btrfs subvolume create /btrfs/@

echo "Create snapshots subvolume"
btrfs subvolume create /btrfs/@snapshots

echo "Create home and srv subvolumes"
btrfs subvolume create /btrfs/@home
btrfs subvolume create /btrfs/@home/andrei
btrfs subvolume create /btrfs/@srv
