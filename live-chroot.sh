#!/bin/bash

# archiso
systemctl start sshd
passwd

# From host
# Copy SSH key
ssh-copy-id -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no root@192.168.122.5
# Copy scripts
scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -r ./scripts root@192.168.122.5:/root/install-scripts

# Enter
cryptsetup open /dev/vda2 root
mount -o subvol=@ /dev/mapper/root /mnt
mount /dev/vda1 /mnt/boot
mount -o subvol=@home /dev/mapper/root /mnt/home
mount -o subvol=@srv /dev/mapper/root /mnt/srv

mount -o subvol=@snapshots /dev/mapper/root /mnt/.snapshots

arch-chroot /mnt

# systemd-nspawn

systemd-nspawn --boot --directory=/mnt


# Unmount
umount /mnt/boot
umount /mnt/home
umount /mnt/srv
umount /mnt/.snapshots
umount /mnt
